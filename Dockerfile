# Prebuilt image support cross-compiling from Linux to MacOS
FROM rust:1.75.0

RUN apt update && apt install cmake clang lzma-dev libxml2-dev -y

ENV MACOSX_CROSS_COMPILER=/macosx-cross-compiler
RUN mkdir -p $MACOSX_CROSS_COMPILER/cross-compiler

# Clone toolchain build scripts
WORKDIR $MACOSX_CROSS_COMPILER
RUN git clone https://github.com/tpoechtrager/osxcross

# Download prebuilt MacOSX SDK
RUN wget https://github.com/phracker/MacOSX-SDKs/releases/download/11.3/MacOSX11.3.sdk.tar.xz -P osxcross/tarballs

# Build MacOS toolchain
RUN UNATTENDED=yes OSX_VERSION_MIN=10.15.7 TARGET_DIR=$MACOSX_CROSS_COMPILER/cross-compiler ./osxcross/build.sh
ENV PATH=$MACOSX_CROSS_COMPILER/cross-compiler/bin:$PATH

# Add additional x64 targets
RUN rustup target add x86_64-apple-darwin aarch64-apple-darwin

RUN printf "[target.x86_64-apple-darwin]\n\
linker = \"x86_64-apple-darwin20.4-clang\"\n\
ar = \"x86_64-apple-darwin20.4-ar\"\n\
\n\
[target.aarch64-apple-darwin]\n\
linker = \"aarch64-apple-darwin20.4-clang\"\n\
ar = \"aarch64-apple-darwin20.4-ar\"" > $CARGO_HOME/config.toml