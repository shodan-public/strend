stages:
  - test
  - build
  - upload
  - release

variables:
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/strend/${CI_COMMIT_TAG}"
  PACKAGE_LATEST_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/strend/latest"

build-cross-image:
    stage: build
    image: docker:git
    services:
      - docker:dind
    script:
      - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
      - docker build -t registry.gitlab.com/${CI_PROJECT_NAMESPACE}/strend/rust-cross:latest .
      - docker push registry.gitlab.com/${CI_PROJECT_NAMESPACE}/strend/rust-cross:latest
    when: manual  # This job takes time, need to trigger in UI and don't often rebuild.

test:
  stage: test
  image: rust:latest
  script:
    - rustup component add rustfmt
    - cargo fmt --all -- --check
    - cargo test -- --nocapture

build-linux:
  stage: build
  image: rust:latest
  script:
    - cargo install cargo-deb cargo-zigbuild cargo-generate-rpm
    - cargo build --release
    - strip target/release/strend
    - mkdir bin
    - cp target/release/strend bin/strend-${CI_COMMIT_TAG}-linux-x86_64
    - cargo deb
    - mv target/debian/strend*.deb bin/strend-${CI_COMMIT_TAG}-x86_64.deb
    - apt update && apt install python3-pip -y
    - pip3 install ziglang --break-system-packages
    # Statically links against MUSL libc so the executable not depends on CentOS glibc
    # https://stackoverflow.com/questions/63724484/build-and-bind-against-older-libc-version
    - rustup target add x86_64-unknown-linux-musl
    - cargo zigbuild --release --target x86_64-unknown-linux-musl
    - strip target/x86_64-unknown-linux-musl/release/strend
    - cargo generate-rpm --target x86_64-unknown-linux-musl --auto-req disabled
    - mv target/x86_64-unknown-linux-musl/generate-rpm/strend*.rpm bin/strend-${CI_COMMIT_TAG}-x86_64.rpm
    - cd bin
    - tar cvzf strend-${CI_COMMIT_TAG}-linux-x86_64.tar.gz *-${CI_COMMIT_TAG}-linux-x86_64
  cache:
    paths:
      - target/release/
  artifacts:
    paths:
      - bin
  rules:
    - if: $CI_COMMIT_TAG

build-macos:
  stage: build
  image: registry.gitlab.com/${CI_PROJECT_NAMESPACE}/strend/rust-cross:latest
  script:
    - CC=o64-clang cargo build --target x86_64-apple-darwin --release
    - CC=oa64-clang cargo build --target aarch64-apple-darwin --release
    - mkdir bin
    - mv target/x86_64-apple-darwin/release/strend bin/strend-${CI_COMMIT_TAG}-darwin-x86_64
    - mv target/aarch64-apple-darwin/release/strend bin/strend-${CI_COMMIT_TAG}-darwin-arm64
    - cd bin
    - tar cvzf strend-${CI_COMMIT_TAG}-darwin-x86_64.tar.gz *-${CI_COMMIT_TAG}-darwin-x86_64
    - tar cvzf strend-${CI_COMMIT_TAG}-darwin-arm64.tar.gz *-${CI_COMMIT_TAG}-darwin-arm64
  cache:
    paths:
      - target/release/
  artifacts:
    paths:
      - bin
  rules:
    - if: $CI_COMMIT_TAG

build-windows:
  stage: build
  image: rust:latest
  script:
    - apt update && apt install zip gcc-mingw-w64-x86-64 -y
    - rustup target add x86_64-pc-windows-gnu
    - cargo build --target x86_64-pc-windows-gnu --release
    - mkdir bin
    - mv target/x86_64-pc-windows-gnu/release/strend.exe bin/strend-${CI_COMMIT_TAG}-windows-x86_64.exe
    - zip bin/strend-${CI_COMMIT_TAG}-windows-x86_64.zip bin/strend-${CI_COMMIT_TAG}-windows-x86_64.exe
  cache:
    paths:
      - target/release/
  artifacts:
    paths:
      - bin
  rules:
    - if: $CI_COMMIT_TAG

upload:
  stage: upload
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/strend-${CI_COMMIT_TAG}-linux-x86_64.tar.gz ${PACKAGE_REGISTRY_URL}/strend-linux-x86_64.tar.gz
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/strend-${CI_COMMIT_TAG}-x86_64.deb ${PACKAGE_REGISTRY_URL}/strend-${CI_COMMIT_TAG}-x86_64.deb
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/strend-${CI_COMMIT_TAG}-x86_64.rpm ${PACKAGE_REGISTRY_URL}/strend-${CI_COMMIT_TAG}-x86_64.rpm
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/strend-${CI_COMMIT_TAG}-darwin-x86_64.tar.gz ${PACKAGE_REGISTRY_URL}/strend-darwin-x86_64.tar.gz
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/strend-${CI_COMMIT_TAG}-darwin-arm64.tar.gz ${PACKAGE_REGISTRY_URL}/strend-darwin-arm64.tar.gz
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/strend-${CI_COMMIT_TAG}-windows-x86_64.zip ${PACKAGE_REGISTRY_URL}/strend-windows-x86_64.zip
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/strend-${CI_COMMIT_TAG}-linux-x86_64.tar.gz ${PACKAGE_LATEST_REGISTRY_URL}/strend-latest-linux-x86_64.tar.gz
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/strend-${CI_COMMIT_TAG}-x86_64.deb ${PACKAGE_LATEST_REGISTRY_URL}/strend-latest-x86_64.deb
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/strend-${CI_COMMIT_TAG}-x86_64.rpm ${PACKAGE_LATEST_REGISTRY_URL}/strend-latest-x86_64.rpm
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/strend-${CI_COMMIT_TAG}-darwin-x86_64.tar.gz ${PACKAGE_LATEST_REGISTRY_URL}/strend-latest-darwin-x86_64.tar.gz
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/strend-${CI_COMMIT_TAG}-darwin-arm64.tar.gz ${PACKAGE_LATEST_REGISTRY_URL}/strend-latest-darwin-arm64.tar.gz
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/strend-${CI_COMMIT_TAG}-windows-x86_64.zip ${PACKAGE_LATEST_REGISTRY_URL}/strend-latest-windows-x86_64.zip

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  release:
    name: "Release $CI_COMMIT_TAG"
    description: '$DESCRIPTION'
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_TAG'
    assets:
      links:
        - name: 'strend-linux-x86_64.tar.gz'
          url: "$PACKAGE_REGISTRY_URL/strend-linux-x86_64.tar.gz"
        - name: "strend-${CI_COMMIT_TAG}-x86_64.deb"
          url: "$PACKAGE_REGISTRY_URL/strend-${CI_COMMIT_TAG}-x86_64.deb"
        - name: "strend-${CI_COMMIT_TAG}-x86_64.rpm"
          url: "$PACKAGE_REGISTRY_URL/strend-${CI_COMMIT_TAG}-x86_64.rpm"
        - name: 'strend-darwin-x86_64.tar.gz'
          url: "$PACKAGE_REGISTRY_URL/strend-darwin-x86_64.tar.gz"
        - name: 'strend-darwin-arm64.tar.gz'
          url: "$PACKAGE_REGISTRY_URL/strend-darwin-arm64.tar.gz"
        - name: 'strend-windows-x86_64.zip'
          url: "$PACKAGE_REGISTRY_URL/strend-windows-x86_64.zip"
  script:
    - echo "Creating a release for $CI_COMMIT_TAG"

homebrew:
  stage: release
  image: rust:latest
  variables:
    # $CI_JOB_TOKEN doesn't have write repository permission, so we create a Personal Token and put it in project Settings > CI/CD > Variables (Uncheck Protect variable)
    TAP_REPOSITORY_URL: "https://gitlab-ci-push-token:${CI_PUSH_TOKEN}@gitlab.com/shodan-public/homebrew-shodan.git"
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - apt update && apt install git -y
    - x86_64_sha=$(sha256sum bin/strend-${CI_COMMIT_TAG}-darwin-x86_64.tar.gz | awk '{ print $1 }')
    - arm64_sha=$(sha256sum bin/strend-${CI_COMMIT_TAG}-darwin-arm64.tar.gz | awk '{ print $1 }')
    - git clone $TAP_REPOSITORY_URL && cd homebrew-shodan

    # Update package version and checksums
    - sed -i "s/sha256 \".*\"/sha256 \"$x86_64_sha\"/1" Formula/strend.rb
    - sed -i -z "s/sha256 \".*\"/sha256 \"$arm64_sha\"/m2" Formula/strend.rb
    - sed -i "s/version \".*\"/version \"$CI_COMMIT_TAG\"/g" Formula/strend.rb
    - git add .
    - git -c user.email="${GITLAB_USER_EMAIL}" -c user.name="${GITLAB_USER_NAME}" commit -m "release:\ strend ${CI_COMMIT_TAG}"
    - git --no-pager show HEAD
    - git push $TAP_REPOSITORY_URL HEAD:main  # Gitlab CI is in the detached HEAD state, so must set a branch to push

cargo-publish:
  stage: release
  image: rust:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      # Get latest version in crates.io and compare before publish
      latest_version=$(cargo search strend | cut -d "#" -f 1 | cut -d "=" -f 2 | sed -e 's/^[[:space:]]*//' | xargs)
      if [ "$latest_version" != "${CI_COMMIT_TAG}" ]; then
        cargo publish --token "${CARGO_REGISTRY_TOKEN}"
      else
        echo -e "\033[33mWARNING: ${CI_COMMIT_TAG} version already existed\033[0m"
      fi